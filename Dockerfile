FROM python:3.8-buster

RUN pip install pipenv

WORKDIR /home/nashirdjango
COPY Pipfile Pipfile.lock ./
RUN pipenv install --system --deploy --ignore-pipfile

COPY nashirdjango/ nashirdjango/

EXPOSE 5000

WORKDIR /home/nashirdjango/nashirdjango
CMD ["daphne", "--bind", "0.0.0.0", "--port", "5000", "nashirdjango.asgi:application"]
