# Nashirdjango documentation

## Dev environment setup

Install [pyenv](https://github.com/pyenv/pyenv) and [pipenv](https://pipenv.pypa.io)

### Useful commands

```sh
# pipenv
cd nashirdjango
pipenv install pkg_name
pipenv shell # activates the virtual env
```

## Nashirdjando docker image

### Building

```sh
# Example for building the image
DOCKER_BUILDKIT=1 docker build -t myrepo/nashirdjango:0.0.1 -t myrepo/nashirdjango:latest .
```

### Usage

Normally for production, you use your django image in a container cluster like kubernetes, but for a quick start, you can read the following which explains how to launch django using `docker run`

```sh
docker run -d -p 5000:5000 myrepo/nashirdjango
```

The above will spin up django in "development" environment (settings read from settings/development.py)

For other environments (staging, production...), some "env variables" need to be provided. The following is an example for the staging environment.

```sh
docker network create --driver bridge apps-net
docker run -d -p 5000:5000 --name django --network apps-net \
    -e DJANGO_ENV=staging \
    -e DJANGO_SECRET_KEY='wzlc18-&+40z#+%dad)hi)1hg#qn25xz9!)inm3-pitdgy%nse' \
    -e DJANGO_ALLOWED_HOSTS='127.0.0.1 localhost django' \
    -e DJANGO_DATABASE_NAME=my_django_db_name \
    -e DJANGO_DATABASE_USER=my_django_db_user \
    -e DJANGO_DATABASE_PASSWORD='my_django_db_pass' \
    -e DJANGO_DATABASE_HOST=postgres \
    myrepo/nashirdjango
```

#### Secret key

You can generate a django SECRET_KEY with the following:

```sh
docker run --rm myrepo/nashirdjango python -c '
from django.core.management.utils import get_random_secret_key
print(get_random_secret_key())
'
```

#### Database

If needed, you can get a postgres server running using:

```sh
mkdir -p ~/pgdata
docker run -d --name postgres --network apps-net \
    -e POSTGRES_PASSWORD=mysecretpassword \
    -v ~/pgdata:/var/lib/postgresql/data \
    -p 5432:5432 \
    postgres:alpine

# access psgl
docker exec -it postgres psql -U postgres

# inside psgl you can create a user and a db
CREATE USER my_django_db_user WITH PASSWORD 'my_django_db_pass';
CREATE DATABASE my_django_db_name OWNER my_django_db_user;
```

#### Static files

To collect django static files:

```sh
mkdir -p ~/django-static-files
docker run --rm --network apps-net \
    -v ~/django-static-files:/home/nashirdjango/nashirdjango/static \
    -e DJANGO_ENV=staging \
    -e DJANGO_SECRET_KEY='wzlc18-&+40z#+%dad)hi)1hg#qn25xz9!)inm3-pitdgy%nse' \
    -e DJANGO_ALLOWED_HOSTS='127.0.0.1 localhost' \
    -e DJANGO_DATABASE_NAME=my_django_db_name \
    -e DJANGO_DATABASE_USER=my_django_db_user \
    -e DJANGO_DATABASE_PASSWORD='my_django_db_pass' \
    -e DJANGO_DATABASE_HOST=postgres \
    myrepo/nashirdjango python manage.py collectstatic --noinput
```

To serve the static files, you can start an nginx container. nginx will directly serve the static files, and will proxy other requests to django container.

```sh
# example nginx config
cat <<EOF > ~/nginx-django.conf
upstream django {
    server django:5000;
}
server {
    listen 80;
    location /static {
        alias /django/static;
    }
    location / {
        proxy_pass http://django;
    }
}
EOF

# start nginx
docker run -d --name nginx --network apps-net \
    -v ~/nginx-django.conf:/etc/nginx/conf.d/default.conf \
    -v ~/django-static-files:/django/static \
    -p 80:80 \
    nginx:alpine
```

The above is an example setup to explain how things work at a basic level, it is not suitable for production.

#### Django DEBUG

You can provide the "DJANGO_DEBUG" env variable to set the django "DEBUG" setting. (default is `True`)

```sh
# disable DEBUG in production
docker run ...
    -e DJANGO_DEBUG=False
    ...
```
