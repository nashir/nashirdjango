# Settings common between all environments

from .base import *


# Settings used for all deployments (staging, production, ...)

SECRET_KEY = os.environ['DJANGO_SECRET_KEY']

import dj_database_url
DATABASES['default'] = dj_database_url.config()

ALLOWED_HOSTS = os.environ['DJANGO_ALLOWED_HOSTS'].split()

SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_BROWSER_XSS_FILTER = True
CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True
X_FRAME_OPTIONS = 'SAMEORIGIN'
