# Settings common between all environments

from .base import *
from .deployment import *


# Environment specific settings
