# Settings common between all environments

from .base import *
from .deployment import *


# Environment specific settings

#SECURE_SSL_REDIRECT = True
#SECURE_HSTS_SECONDS = 31536000
#SECURE_HSTS_INCLUDE_SUBDOMAINS = False
#SECURE_HSTS_PRELOAD = True
